﻿using Connector.Events;
using Connector.Packets;
using System;
using System.Threading;
using GameNetwork;

namespace ConnectorTestServer
{
    class Server : IEventListener
    {
        public static void Main(string[] args)
        {
            new Server();
        }

        public Server()
        {
            Console.WriteLine("------Server------");

            new GameServer(35567, 35568);
        }
    }
}
