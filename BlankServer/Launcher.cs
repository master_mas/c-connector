﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Connector;
using Connector.Events;
using GameNetwork.Packets;

namespace BlankServer
{
    public class Launcher : IEventListener
    {

        public static void Main(string[] args)
        {
            new Launcher();
        }

        public Launcher()
        {
            Event.registerListener(this);

            Console.WriteLine("------Server------");

            ConnectorServer server = new ConnectorServer(35567, 35568);

            server.registerMessageHandler(2, new CreateObjectHandler(this).getDelegate());
            server.registerMessageHandler(3, new DestroyObjectHandler(this).getDelegate());

            new Thread(new ThreadStart(server.run)).Start();

            Console.WriteLine("Press Enter to Stop Server");
            Console.ReadLine();
            Console.WriteLine("Stopping Server");
            server.cleanup();
        }

        [Connector.Events.EventHandler]
        public void connection(EventClientConnection e)
        {
            Event.call(new EventLogging("Client has connected on " + e.getIPAddress() + " and is known as " + e.getClientHandle().getClientID()));
        }

        [Connector.Events.EventHandler]
        public void disconnect(EventClientDisconnect e)
        {
            Event.call(new EventLogging("Client: " + e.getClientID() + " has disconnected"));
        }

        [Connector.Events.EventHandler]
        public void log(EventLogging e)
        {
            Console.WriteLine(e);
        }

        private Queue<int> waitingIDs = new Queue<int>();
        private int upTo = 0;

        public void release(int id)
        {
            waitingIDs.Enqueue(id);
        }

        public int get()
        {
            if (waitingIDs.Count > 0)
                return waitingIDs.Dequeue();
            else
                return upTo++;
        }

        private class CreateObjectHandler : ConnectorMessageHandler<MessageCreateObject>
        {
            private readonly Launcher _launcher;

            public CreateObjectHandler(Launcher launcher) : base(2)
            {
                _launcher = launcher;
            }

            public override void handleMessage(int id, MessageCreateObject data, IConnectionSender sender)
            {
                data.setNetworkObjectID(_launcher.get());
                sender.send(data);
            }
        }

        private class DestroyObjectHandler : ConnectorMessageHandler<MessageDestoryObject>
        {
            private readonly Launcher _launcher;

            public DestroyObjectHandler(Launcher launcher) : base(3)
            {
                _launcher = launcher;
            }

            public override void handleMessage(int id, MessageDestoryObject data, IConnectionSender sender)
            {
                _launcher.release(data.getNetworkObjectID());
            }
        }
    }
}
