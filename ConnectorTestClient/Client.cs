﻿using Connector.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConnectorTestClient
{
    class Client : Connector.Events.IEventListener
    {

        public static void Main(string[] args)
        {
            new Client();
        }

        public Client()
        {
            Connector.Events.Event.registerListener(this);
            Test test;
            Event.registerListener(test = new Test());
            Event.call(new EventClientConnection(null, null));
            Event.removeListener(test);
            Event.call(new EventClientConnection(null, null));

            Console.ReadLine();
            Thread.Sleep(1000);

            Environment.Exit(0);
        }

        [Connector.Events.EventHandler]
        public void log(EventLogging e)
        {
            Console.WriteLine(e.getLogMessage());
        }

        private class Test : IEventListener
        {
            [Connector.Events.EventHandler]
            public void hello(EventClientConnection e)
            {
                Event.call(new EventLogging("Hello"));
            }
        }
    }
}
