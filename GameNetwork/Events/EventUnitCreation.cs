﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Connector.Events;
using GameNetwork.Objects;

namespace GameNetwork.Events
{
    public class EventUnitCreation : EventEntity, ICancellable
    {
        private static EventHandlerContainer hc = new EventHandlerContainer();

        private bool cancel = false;
        private readonly Player creator;

        public EventUnitCreation(NetworkType type, Player creator) : base(null)
        {
            this.creator = creator;
        }

        public bool isCancelled()
        {
            return cancel;
        }

        public void setCancelled(bool cancel)
        {
            this.cancel = cancel;
        }

        public Player getCreator()
        {
            return creator;
        }
    }
}
