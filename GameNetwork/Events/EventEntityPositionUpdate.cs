﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Connector.Events;
using GameNetwork.Objects;

namespace GameNetwork.Events
{
    public class EventEntityPositionUpdate : EventEntity, ICancellable
    {
        private static EventHandlerContainer hc = new EventHandlerContainer();

        private bool cancel = false;

        public EventEntityPositionUpdate(GameObject entity) : base(entity)
        {
        }

        public bool isCancelled()
        {
            return cancel;
        }

        public void setCancelled(bool cancel)
        {
            this.cancel = cancel;
        }
    }
}
