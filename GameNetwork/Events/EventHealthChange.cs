﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Connector.Events;

namespace GameNetwork.Events
{
    public class EventHealthChange : Event, ICancellable
    {
        private static EventHandlerContainer hc = new EventHandlerContainer();

        private readonly float amount;
        private readonly int networkObjectID;

        private bool cancel = false;

        public EventHealthChange(float amount, int networkObjectID)
        {
            this.amount = amount;
            this.networkObjectID = networkObjectID;
        }

        public float Amount => amount;

        public int NetworkObjectId => networkObjectID;
        public bool isCancelled()
        {
            return cancel;
        }

        public void setCancelled(bool cancel)
        {
            this.cancel = cancel;
        }
    }
}
