﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Connector.Events;

namespace GameNetwork.Events
{
    public class EventPlayer : Event
    {
        private Player player;

        public EventPlayer(Player who)
        {
            this.player = who;
        }

        public Player getPlayer()
        {
            return player;
        }
    }
}
