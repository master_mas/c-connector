﻿using Connector.Events;
using UnityEngine;
using Connector.Packets;

namespace GameNetwork.Events
{
    public class EventUpdatePosition : Connector.Events.Event, Connector.Events.ICancellable
    {
        private EventHandlerContainer hc = new EventHandlerContainer();

        private Vector3 position;
        private Vector3 eulerAngles;

        private bool targetingObject;
        private int targetNetworkObjectID;
        private Vector3 targetPosition;

        private Vector3 velocity;

        private bool cancel = false;
        
        public EventUpdatePosition(Vector3 position, Vector3 eulerAngles, bool targetingObject, int targetNetworkObjectId, Vector3 targetPosition, Vector3 velocity) 
        {
            this.position = position;
            this.eulerAngles = eulerAngles;
            this.targetingObject = targetingObject;
            targetNetworkObjectID = targetNetworkObjectId;
            this.targetPosition = targetPosition;
            this.velocity = velocity;
        }

        public bool isCancelled()
        {
            return cancel;
        }

        public void setCancelled(bool cancel)
        {
            this.cancel = cancel;
        }
    }
}
