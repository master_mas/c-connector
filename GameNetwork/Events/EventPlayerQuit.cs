﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Connector.Events;

namespace GameNetwork.Events
{
    public class EventPlayerQuit : EventPlayer
    {
        private static EventHandlerContainer hc = new EventHandlerContainer();

        public EventPlayerQuit(Player who) : base(who)
        {
        }
    }
}
