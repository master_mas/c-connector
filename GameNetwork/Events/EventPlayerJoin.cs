﻿using Connector.Events;

namespace GameNetwork.Events
{
    public class EventPlayerJoin : EventPlayer, ICancellable
    {
        private static EventHandlerContainer hc = new EventHandlerContainer();

        private bool cancel = false;

        public EventPlayerJoin(Player who) : base(who) { }

        public bool isCancelled()
        {
            return cancel;
        }

        public void setCancelled(bool cancel)
        {
            this.cancel = cancel;
        }
    }
}
