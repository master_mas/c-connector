﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Connector.Events;

namespace GameNetwork.Events
{
    public class EventGameObjectDestroyed : Event
    {
        private static EventHandlerContainer hc = new EventHandlerContainer();

        private readonly int networkID;

        public EventGameObjectDestroyed(int networkId)
        {
            networkID = networkId;
        }

        public int getNetworkID()
        {
            return networkID;
        }
    }
}
