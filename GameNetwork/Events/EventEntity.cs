﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Connector.Events;
using GameNetwork.Objects;

namespace GameNetwork.Events
{
    public class EventEntity : Event
    {
        private GameObject entity;

        public EventEntity(GameObject entity)
        {
            this.entity = entity;
        }

        public GameObject getEntity()
        {
            return entity;
        }
    }
}
