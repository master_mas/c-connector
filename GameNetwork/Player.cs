﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Connector;
using GameNetwork.Packets;

namespace GameNetwork
{
    public class Player
    {
        private readonly int playerID;
        //Try to keep this hidden;
        private readonly ConnectorClientHandle handle;

        private Team team = null;

        public struct Unsafe
        {
            public readonly ConnectorClientHandle handle;

            public Unsafe(ConnectorClientHandle handle)
            {
                this.handle = handle;
            }
        }
        private readonly Unsafe unsafeContainer;

        public Player(ConnectorClientHandle handle)
        {
            this.handle = handle;
            this.playerID = handle.getClientID();
            unsafeContainer = new Unsafe(handle);
        }

        public void send(GameMessage message)
        {
            handle.send(message);
        }

        public Unsafe getUnsafe()
        {
            return unsafeContainer;
        }

        public int getPlayerID()
        {
            return playerID;
        }

        public void setTeam(Team team)
        {
            this.team = team;
        }

        public int getTeamID()
        {
            return team.getTeamID();
        }

        public override string ToString()
        {
            return "Player[ID:" + playerID + ",Team:" + team.getTeamID() + ",Address:" + handle.getEndpoint().Address + "]";
        }
    }
}
