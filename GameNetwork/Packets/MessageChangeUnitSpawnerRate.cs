﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameNetwork.Packets
{
    [Serializable]
    public class MessageChangeUnitSpawnerRate : GameMessage
    {
        private readonly int _time;

        public MessageChangeUnitSpawnerRate(int time) : base(GameMessageIDs.CHANGE_UNTI_SPAWNER_TIME, -1)
        {
            _time = time;
        }

        public int Time => _time;
    }
}
