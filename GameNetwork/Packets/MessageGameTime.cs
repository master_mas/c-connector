﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameNetwork.Packets
{
    [Serializable]
    public class MessageGameTime : GameMessage
    {
        private int gameTime;

        public MessageGameTime(int gameTime) : base(GameMessageIDs.GAME_TME, -1)
        {
            this.gameTime = gameTime;
        }

        public int GameTime => gameTime;
    }
}
