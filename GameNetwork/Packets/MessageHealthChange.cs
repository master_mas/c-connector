﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameNetwork.Packets
{
    [Serializable]
    public class MessageHealthChange : GameMessage
    {
        private float amount;

        public MessageHealthChange(int networkObjectID, float amount) : base(GameMessageIDs.MOD_HEALTH, networkObjectID)
        {
            this.amount = amount;
        }

        public float getAmount()
        {
            return amount;
        }

        public void setAmount(float amount)
        {
            this.amount = amount;
        }
    }
}
