﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Connector.Events;

namespace GameNetwork.Packets
{
    [Serializable]
    public class MessagePlayerData : GameMessage
    {
        private readonly int teamID;
        private readonly int playerID;

        public MessagePlayerData(int teamId, int playerID) : base(GameMessageIDs.PLAYER_DATA_OTHER, -1)
        {
            teamID = teamId;
            this.playerID = playerID;
            setBackBone(true);
        }

        public int getTeamID()
        {
            return teamID;
        }

        public int getPlayerID()
        {
            return playerID;
        }
    }
}
