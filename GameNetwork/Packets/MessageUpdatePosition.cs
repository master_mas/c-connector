﻿using System;
using GameNetwork.Serialization;
using UnityEngine;

namespace GameNetwork.Packets
{
    [Serializable]
    public class MessageUpdatePosition : GameMessage, Connector.Events.ICancellable
    {
        [Serializable]
        public enum Reason
        {
            MOVEMENT,
            DEATH,
            STARTING,
            TELEPORT
        }

        private float[] position;
        private float[] eulerAngles;
        private float[] velocity;

        private int timeSync;

        private readonly Reason reason;
        private bool cancel;

        public MessageUpdatePosition(int networkObjectID, Vector3 position, Vector3 eulerAngles, Vector3 velocity, Reason reason) : base(GameMessageIDs.UDPATE_POSITON, networkObjectID)
        {
            this.position = Vector3Serialize.serialize(position);
            this.eulerAngles = Vector3Serialize.serialize(eulerAngles);
            this.velocity = Vector3Serialize.serialize(velocity);
            this.reason = reason;
            timeSync = DateTime.UtcNow.Millisecond;
        }

        public bool isCancelled()
        {
            return cancel;
        }

        public void setCancelled(bool cancel)
        {
            this.cancel = cancel;
        }

        public Reason getReason()
        {
            return reason;
        }

        public Vector3 getPosition()
        {
            return Vector3Serialize.deserialize(position);
        }

        public Vector3 getEulerAngles()
        {
            return Vector3Serialize.deserialize(eulerAngles);
        }

        public Vector3 getVelocity()
        {
            return Vector3Serialize.deserialize(velocity);
        }

        public void setVelocity(float[] velocity)
        {
            this.velocity = velocity;
        }

        public void setPositon(float[] position)
        {
            this.position = position;
        }

        public void setRotation(float[] rotation)
        {
            this.eulerAngles = rotation;
        }

        public int getTimeSync()
        {
            return timeSync;
        }
    }
}
