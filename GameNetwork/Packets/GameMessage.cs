﻿using System;
using Connector;

namespace GameNetwork.Packets
{
    [Serializable]
    public class GameMessage : ConnectorMessage
    {
        protected int networkObjectID;

        public GameMessage(GameMessageIDs messageID, int networkObjectID) : base((int)messageID)
        {
            this.networkObjectID = networkObjectID;
        }

        public int getNetworkObjectID()
        {
            return networkObjectID;
        }

        public void setNetworkObjectID(int networkObjectID)
        {
            this.networkObjectID = networkObjectID;
        }
    }
}
