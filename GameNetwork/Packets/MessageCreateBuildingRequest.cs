﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameNetwork.Serialization;
using UnityEngine;

namespace GameNetwork.Packets
{
    [Serializable]
    public class MessageCreateBuildingRequest : GameMessage
    {
        private readonly NetworkType type;
        private readonly float[] position;
        private readonly float[] rotation;

        private readonly NetworkType unitType;

        public MessageCreateBuildingRequest(NetworkType type, Vector3 position, Vector3 rotation, NetworkType unitType = NetworkType.TANK_SCOUT) : base(GameMessageIDs.CREATE_BUILDING_REQUEST, -1)
        {
            this.type = type;
            this.position = Vector3Serialize.serialize(position);
            this.rotation = Vector3Serialize.serialize(rotation);
            this.unitType = unitType;
        }

        public NetworkType Type => type;

        public Vector3 Position => Vector3Serialize.deserialize(position);

        public Vector3 Rotation => Vector3Serialize.deserialize(rotation);

        public NetworkType UnitType => unitType;
    }
}
