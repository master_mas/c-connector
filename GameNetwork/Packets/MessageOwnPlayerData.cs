﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameNetwork.Packets
{
    [Serializable]
    public class MessageOwnPlayerData : GameMessage
    {
        private readonly int teamID;
        private readonly int playerID;

        public MessageOwnPlayerData(int teamID, int playerID) : base(GameMessageIDs.PLAYER_DATA_OWN, -1)
        {
            this.teamID = teamID;
            this.playerID = playerID;
            setBackBone(true);
        }

        public int getTeamID()
        {
            return teamID;
        }

        public int getPlayerID()
        {
            return playerID;
        }
    }
}
