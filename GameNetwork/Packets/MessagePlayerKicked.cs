﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameNetwork.Packets
{
    [Serializable]
    public class MessagePlayerKicked : GameMessage
    {
        private readonly string message;
        private readonly KickedType reason;

        public MessagePlayerKicked(string message, KickedType reason) : base(GameMessageIDs.PLAYER_KICK, -1)
        {
            this.message = message;
            this.reason = reason;
            setBackBone(true);
        }

        public string getMessage()
        {
            return message;
        }

        public KickedType getReason()
        {
            return reason;
        }
    }
}
