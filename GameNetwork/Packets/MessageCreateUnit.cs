﻿using System;
using GameNetwork.Serialization;
using UnityEngine;

namespace GameNetwork.Packets
{
    [Serializable]
    public class MessageCreateUnit : GameMessage
    {
        private readonly int teamID;
        private readonly NetworkType type;

        private readonly float[] position;
        private readonly float[] rotation;

        private readonly int networkSpawnParent;

        public MessageCreateUnit(int networkID, int teamID, NetworkType type, Vector3 position, Vector3 rotation, int networkParent) : base(GameMessageIDs.UNIT_CREATION, networkID)
        {
            this.teamID = teamID;
            this.type = type;
            this.position = Vector3Serialize.serialize(position);
            this.rotation = Vector3Serialize.serialize(rotation);
            this.networkSpawnParent = networkParent;
            setBackBone(true);
        }

        public int TeamId => teamID;

        public NetworkType Type => type;

        public Vector3 Position => Vector3Serialize.deserialize(position);

        public Vector3 Rotation => Vector3Serialize.deserialize(rotation);

        public int NetworkSpawnParent => networkSpawnParent;
    }
}
