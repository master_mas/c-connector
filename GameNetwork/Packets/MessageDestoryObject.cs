﻿using System;

namespace GameNetwork.Packets
{
    [Serializable]
    public class MessageDestoryObject : GameMessage
    {
        public MessageDestoryObject(int networkObjectID) : base(GameMessageIDs.DESTORY_OBJECT, networkObjectID)
        {
            setBackBone(true);
        }
    }
}
