﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameNetwork.Packets
{
    public class MessageResourceCollected : GameMessage
    {
        private readonly float resource;

        public MessageResourceCollected(float addedResource) : base(GameMessageIDs.RESOURCE_COLLECT, -1)
        {
            this.resource = addedResource;
        }

        public float Resource => resource;
    }
}
