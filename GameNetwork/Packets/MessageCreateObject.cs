﻿using System;
using GameNetwork.Serialization;
using UnityEngine;

namespace GameNetwork.Packets
{
    [System.Serializable]
    public class MessageCreateObject : GameMessage, Connector.Events.ICancellable
    {
        private readonly int teamID;
        private readonly int playerID;
        private readonly NetworkType type;
        [Obsolete]
        private int internalID;
        private readonly float[] position;
        private readonly float[] rotation;

        private bool cancel = false;

        public MessageCreateObject(int networkObjectID, int teamID, NetworkType type, Vector3 position, Vector3 rotation, int playerID) : base(GameMessageIDs.CREATE_OBJECT, networkObjectID)
        {
            this.teamID = teamID;
            this.playerID = playerID;
            this.type = type;
            this.position = Vector3Serialize.serialize(position);
            this.rotation = Vector3Serialize.serialize(rotation);
//            setBackBone(true);
        }

        public bool isCancelled()
        {
            return cancel;
        }

        public void setCancelled(bool cancel)
        {
            this.cancel = cancel;
        }

        public NetworkType getObjectType()
        {
            return type;
        }

        [Obsolete]
        public void setInternalID(int id)
        {
            this.internalID = id;
        }

        [Obsolete]
        public int getInternalID()
        {
            return internalID;
        }

        public Vector3 getPosition()
        {
            return Vector3Serialize.deserialize(position);
        }

        public Vector3 getRotation()
        {
            return Vector3Serialize.deserialize(rotation);
        }

        public int getTeamID()
        {
            return teamID;
        }

        public int getPlayerID()
        {
            return playerID;
        }
    }
}
