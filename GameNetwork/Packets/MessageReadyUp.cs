﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameNetwork.Packets
{
    [Serializable]
    public class MessageReadyUp : GameMessage
    {
        private readonly int playerID;

        public MessageReadyUp(int playerID) : base(GameMessageIDs.PLAYER_READY_UP, -1)
        {
            this.playerID = playerID;
            setBackBone(true);
        }

        public int getPlayerID()
        {
            return playerID;
        }
    }
}
