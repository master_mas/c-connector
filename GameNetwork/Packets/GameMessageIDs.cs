﻿namespace GameNetwork.Packets
{
    public enum GameMessageIDs
    {
        //Object Transformation
        UDPATE_POSITON = 1,
        CREATE_OBJECT = 2,
        DESTORY_OBJECT = 3,
        UNIT_CREATION = 4,
        CREATE_FACTORY = 5,
        CREATE_BUILDING_REQUEST = 6,

        //Player
        PLAYER_KICK = 100,
        PLAYER_DATA_OWN = 101,
        PLAYER_DATA_OTHER = 102,
        PLAYER_READY_UP = 103,

        //Object Data
        MOD_HEALTH = 200,

        //OTHER
        GAME_TME = 300,
        RESOURCE_COLLECT = 301,
        CHANGE_UNTI_SPAWNER_TIME = 302

    }
}
