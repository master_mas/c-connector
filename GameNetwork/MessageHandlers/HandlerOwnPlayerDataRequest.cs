﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameNetwork.Handlers;
using GameNetwork.Packets;

namespace GameNetwork.MessageHandlers
{
    public class HandlerOwnPlayerDataRequest : ServerMessageHandler<MessageOwnPlayerData>
    {
        public HandlerOwnPlayerDataRequest() : base(GameMessageIDs.PLAYER_DATA_OWN, GameServer.instance(), GameServer.instance().getObjectManagement()) { }

        protected override void handleMessage(GameMessageIDs id, MessageOwnPlayerData data, Player player)
        {
            player.send(new MessageOwnPlayerData(player.getTeamID(), player.getPlayerID()));
        }
    }
}
