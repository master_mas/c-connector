﻿using Connector.Events;
using GameNetwork.Events;
using GameNetwork.Handlers;
using GameNetwork.Objects;
using GameNetwork.Packets;
using MessageUpdatePosition = GameNetwork.Packets.MessageUpdatePosition;

namespace GameNetwork.MessageHandlers
{
    public class HandlerUpdatePosition : ServerMessageHandler<MessageUpdatePosition>
    {
        public HandlerUpdatePosition(GameServer controller) : base(GameMessageIDs.UDPATE_POSITON, controller, controller.getObjectManagement()) { }

        protected override void handleMessage(GameMessageIDs id, MessageUpdatePosition data, Player player)
        {
            GameObject obj = objectManagement.getObject(data.getNetworkObjectID());
            if (obj == null)
                return;
//            bool result = obj.getTransform()
//                .pendPositionUpdate(data.getPosition(), data.getVelocity(), data.getReason());

            bool result = true;

            if (result)
            {
//                if (obj.GetType().IsAssignableFrom(typeof(Entity)))
//                {
//                    result = !Event.call(new EventEntityPositionUpdate((Entity) obj)).isCancelled();
//                }
            }

            if (result)
            {
                obj.getTransform().updateTransform(data.getPosition(), data.getEulerAngles(), data.getVelocity());
                server.broadcast(data, player);
            }
            else
            {
                data.setVelocity(obj.getTransform().getVelocity());
                data.setPositon(obj.getTransform().getPosition());
                data.setRotation(obj.getTransform().getRotation());
                player.send(data);
            }
        }
    }
}
