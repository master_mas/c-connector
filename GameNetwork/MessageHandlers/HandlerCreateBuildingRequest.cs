﻿using GameNetwork.Handlers;
using GameNetwork.Objects.Building;
using GameNetwork.Packets;

namespace GameNetwork.MessageHandlers
{
    public class HandlerCreateBuildingRequest : ServerMessageHandler<MessageCreateBuildingRequest>
    {
        public HandlerCreateBuildingRequest() : base(GameMessageIDs.CREATE_BUILDING_REQUEST, GameServer.instance(), GameServer.instance().getObjectManagement()) { }

        protected override void handleMessage(GameMessageIDs id, MessageCreateBuildingRequest data, Player player)
        {
            switch (data.Type)
            {
                case NetworkType.BUILDING_UNIT_SPAWNER:
                    BuildingUnitSpawner unitSpawner = new BuildingUnitSpawner(data.Position, data.UnitType, 5000, server.getTeam(player.getTeamID()));

                    MessageCreateFactory createFactory = new MessageCreateFactory(unitSpawner.getNetworkID(),
                        unitSpawner.getTeam().getTeamID(), unitSpawner.getNetworkType(),
                        unitSpawner.getTransform().getPositionV(), unitSpawner.getTransform().getRotationV(),
                        player.getPlayerID(), unitSpawner.getUnitType());

                    server.broadcast(createFactory);
                    break;
                case NetworkType.BUILDING_TOWER:
                    BuildingGateTower gateTower = new BuildingGateTower(server.getTeam(player.getTeamID()));

                    MessageCreateObject createObject = new MessageCreateObject(gateTower.getNetworkID(),
                        gateTower.getTeam().getTeamID(), gateTower.getNetworkType(),
                        gateTower.getTransform().getPositionV(), gateTower.getTransform().getRotationV(),
                        player.getPlayerID());
                    break;
                case NetworkType.BUILDING_RESORURCE_COLLECTOR:
                    break;
                default:
                    break;
            }
        }
    }
}
