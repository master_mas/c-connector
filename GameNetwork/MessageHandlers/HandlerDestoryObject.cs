﻿using GameNetwork.Handlers;
using GameNetwork.Objects;
using GameNetwork.Packets;

namespace GameNetwork.MessageHandlers
{
    public class HandlerDestoryObject : ServerMessageHandler<MessageDestoryObject>
    {
        public HandlerDestoryObject(GameServer controller) : base(GameMessageIDs.DESTORY_OBJECT, controller, controller.getObjectManagement())
        {
        }

        protected override void handleMessage(GameMessageIDs id, MessageDestoryObject data, Player player)
        {
            GameObject obj = objectManagement.getObject(data.getNetworkObjectID());
            obj?.death();
        }
    }
}
