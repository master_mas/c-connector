﻿using Connector;
using GameNetwork.Packets;

namespace GameNetwork.Handlers
{
    public abstract class ServerMessageHandler<T> : ConnectorMessageHandler<T>
    {
        protected readonly GameServer server;
        protected readonly ObjectManagement objectManagement;

        protected ServerMessageHandler(GameMessageIDs id, GameServer server, ObjectManagement objectManagement) : base((int)id)
        {
            ConnectorNetworkBase.instance().registerMessageHandler(getID(), getDelegate());

            this.server = server;
            this.objectManagement = objectManagement;
        }

        public override void handleMessage(int id, T data, IConnectionSender sender)
        {
            handleMessage((GameMessageIDs)id, data, server.getPlayer(((ConnectorClientHandle)sender).getClientID()));
        }

        protected abstract void handleMessage(GameMessageIDs id, T data, Player player);
    }
}
