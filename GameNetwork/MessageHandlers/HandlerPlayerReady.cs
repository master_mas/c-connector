﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameNetwork.Handlers;
using GameNetwork.Packets;

namespace GameNetwork.MessageHandlers
{
    public class HandlerPlayerReady : ServerMessageHandler<MessageReadyUp>
    {
        public HandlerPlayerReady() : base(GameMessageIDs.PLAYER_READY_UP, GameServer.instance(), GameServer.instance().getObjectManagement()) { }

        protected override void handleMessage(GameMessageIDs id, MessageReadyUp data, Player player)
        {
            MatchController controller = server.getMatchController();
            controller.readyUpPlayer(player);
        }
    }
}
