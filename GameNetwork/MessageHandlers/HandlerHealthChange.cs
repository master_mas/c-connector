﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Connector.Events;
using GameNetwork.Events;
using GameNetwork.Handlers;
using GameNetwork.Packets;

namespace GameNetwork.MessageHandlers
{
    public class HandlerHealthChange : ServerMessageHandler<MessageHealthChange>
    {
        public HandlerHealthChange(GameServer server, ObjectManagement objectManagement) : base(GameMessageIDs.MOD_HEALTH, server, objectManagement) { }

        protected override void handleMessage(GameMessageIDs id, MessageHealthChange data, Player player)
        {
            if (!Event.call(new EventHealthChange(data.getAmount(), data.getNetworkObjectID())).isCancelled())
            {
                objectManagement.getObject(data.getNetworkObjectID()).modHealth(data.getAmount());
            }
        }
    }
}
