﻿using UnityEngine;

namespace GameNetwork.Serialization
{
    public class Vector3Serialize
    {
        public static float[] serialize(Vector3 vector)
        {
            return new float[] {vector.x, vector.y, vector.z};
        }

        public static Vector3 deserialize(float[] vector)
        {
            return new Vector3(vector[0], vector[1], vector[2]);
        }
    }
}
