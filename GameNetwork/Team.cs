﻿using System;
using System.Collections.Generic;
using Connector.Events;
using GameNetwork.Events;
using GameNetwork.Packets;
using EventHandler = Connector.Events.EventHandler;

namespace GameNetwork
{
    public class Team : IDisposable, IEventListener
    {
        private static Queue<int> waitingIDs = new Queue<int>();
        private static int upTo = 0;

        public static int getID()
        {
            if (waitingIDs.Count <= 0)
            {
                return upTo++;
            }
            else
            {
                return waitingIDs.Dequeue();
            }
        }

        public static void releaseID(int id)
        {
            waitingIDs.Enqueue(id);
        }

        private List<int> players;
        private readonly GameServer server;
        private readonly int teamID;

        private List<int> teamObjects;

        public Team(GameServer server)
        {
            this.server = server;
            teamID = getID();
            players = new List<int>();
            teamObjects = new List<int>(500);

            Event.registerListener(this);
        }

        public void add(Player player)
        {
            players.Add(player.getPlayerID());
            player.setTeam(this);
            player.send(new MessageOwnPlayerData(teamID, player.getPlayerID()));
            server.broadcast(new MessagePlayerData(teamID, player.getPlayerID()), player);
        }

        public void remove(int playerID)
        {
            players.Remove(playerID);
        }

        public void remove(Player player)
        {
            players.Remove(player.getPlayerID());
        }

        public List<int> getPlayerList()
        {
            return players;
        }

        public int getTeamID()
        {
            return teamID;
        }

        public int getTeamTotals()
        {
            return players.Count;
        }

        public bool contains(int playerID)
        {
            return players.Contains(playerID);
        }

        public void addObject(int networkID)
        {
            this.teamObjects.Add(networkID);
            server.getObjectManagement().getObject(networkID).setTeam(this);
        }

        public void removeObject(int networkID)
        {
            this.teamObjects.Remove(networkID);
        }

        [EventHandler]
        public void objectDestroyed(EventGameObjectDestroyed e)
        {
            teamObjects.Remove(e.getNetworkID());
        }

        [EventHandler]
        public void onPlayerLeave(EventPlayerQuit e)
        {
            try
            {
                players.Remove(e.getPlayer().getPlayerID());
            }
            catch (NullReferenceException)
            {
                Event.call(new EventLogging("Tried to remove player but player was null",
                    EventLogging.LogLevel.WARNING));
            }
        }

        public void Dispose()
        {
            releaseID(teamID);
        }

        public void broadcast(GameMessage message)
        {
            foreach (int player in players)
            {
                server.getPlayer(player).send(message);
            }
        }

        public bool containsPlayer(int playerID)
        {
            return players.Contains(playerID);
        }
    }
}
