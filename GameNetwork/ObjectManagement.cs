﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Connector.Events;
using GameNetwork.Events;
using GameNetwork.Packets;
using GameObject = GameNetwork.Objects.GameObject;
using EventHandler = Connector.Events.EventHandler;

namespace GameNetwork
{
    public class ObjectManagement : IEventListener
    {
        private Dictionary<int, GameObject> objects = new Dictionary<int, GameObject>(5000);

        private Queue<int> waitingIDs = new Queue<int>();
        private int upTo = 0;

        [MethodImpl(MethodImplOptions.Synchronized)]
        public int getID()
        {
            if (waitingIDs.Count <= 0)
            {
                return upTo++;
            }
            else
            {
                return waitingIDs.Dequeue();
            }
        }

        public ObjectManagement()
        {
            Event.registerListener(this);
        }

        public void releaseID(int id)
        {
            waitingIDs.Enqueue(id);
        }

        public void registerObject(GameObject obj)
        {
            objects.Add(obj.getNetworkID(), obj);
        }

        public void unregisterObject(int networkID)
        {
            objects.Remove(networkID);
            Event.call(new EventGameObjectDestroyed(networkID));
        }

        public void unregisterObject(GameObject gameObject)
        {
            objects.Remove(gameObject.getNetworkID());
            Event.call(new EventGameObjectDestroyed(gameObject.getNetworkID()));
        }

        public GameObject getObject(int networkID)
        {
            if (objects.ContainsKey(networkID))
            {
                return objects[networkID];
            }

            return null;
        }

        [EventHandler(EventHandler.EventPriority.LOW)]
        public void newPlayerTrackedUpdate(EventPlayerJoin e)
        {
            MessageCreateObject msg;
            foreach (GameObject obj in objects.Values)
            {
                msg = new MessageCreateObject(obj.getNetworkID(), obj.getTeam().getTeamID(), 
                    obj.getNetworkType(), obj.getTransform().getPositionV(), obj.getTransform().getRotationV(), 
                    e.getPlayer().getPlayerID());
                e.getPlayer().send(msg);
            }
        }

        [EventHandler]
        public void removePlayerAssets(EventPlayerQuit e)
        {
            if (e.getPlayer() == null)
                return;

            List<GameObject> objs = new List<GameObject>(objects.Values);
            foreach (GameObject obj in objs)
            {
                if (obj.getTeam().getTeamID() == e.getPlayer().getTeamID())
                {
                    obj.death();
                }
            }
            
        }
    }
}
