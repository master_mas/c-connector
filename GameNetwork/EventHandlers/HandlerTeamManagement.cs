﻿using Connector.Events;
using GameNetwork.Events;
using GameNetwork.Objects.Building;
using GameNetwork.Packets;
using UnityEngine;
using Event = Connector.Events.Event;
using EventHandler = Connector.Events.EventHandler;

namespace GameNetwork.EventHandlers
{
    public class HandlerTeamManagement : IEventListener
    {
        private GameServer server;

        public HandlerTeamManagement(GameServer server)
        {
            this.server = server;
            Event.registerListener(this);
        }

        [EventHandler]
        public void onPlayerJoin(EventPlayerJoin e)
        {
            foreach (Player player in server.getPlayers())
            {
                e.getPlayer().send(new MessagePlayerData(player.getTeamID(), player.getPlayerID()));
            }

            Team[] teams = server.getTeams();

            if (teams[0].getTeamTotals() > teams[1].getTeamTotals())
            {
                teams[1].add(e.getPlayer());
            }
            else
            {
                teams[0].add(e.getPlayer());
            }
        }
    }
}
