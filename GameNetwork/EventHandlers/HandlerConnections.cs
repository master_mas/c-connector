﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Connector.Events;
using GameNetwork.Events;
using GameNetwork.Objects.Building;
using GameNetwork.Packets;
using UnityEngine;
using Event = Connector.Events.Event;
using EventHandler = Connector.Events.EventHandler;

namespace GameNetwork.EventHandlers
{
    public class HandlerConnections : IEventListener
    {
        private GameServer server;

        public HandlerConnections(GameServer server)
        {
            this.server = server;
            Connector.Events.Event.registerListener(this);
        }

        [EventHandler]
        public void onClientConnection(EventClientConnection e)
        {
            Player player = new Player(e.getClientHandle());

            if (Event.call(new EventPlayerJoin(player)).isCancelled())
            {
                player.send(new MessagePlayerKicked("Server Full", KickedType.SERVER_FULL));
            }
            else
            {
                Event.call(new EventLogging("Client Connected via " + e.getIPAddress() + " and assigned ID: " +
                                            e.getClientHandle().getClientID()));
                server.addPlayer(player);
            }
        }

        [EventHandler]
        public void onClientDisconnection(EventClientDisconnect e)
        {
            EventPlayerQuit quit = new EventPlayerQuit(server.getPlayer(e.getClientID()));
            Event.call(quit);
            Event.call(new EventLogging("Client: " + e.getClientID() + " disconnected from server"));
            server.removePlayer(server.getPlayer(e.getClientID()));
        }
    }
}
