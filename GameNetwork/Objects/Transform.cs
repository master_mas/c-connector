﻿using System;
using Connector.Events;
using GameNetwork.Packets;
using GameNetwork.Serialization;
using UnityEngine;
using Event = UnityEngine.Event;

namespace GameNetwork.Objects
{
    public class Transform
    {
        private static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        private Vector3 position = Vector3.zero;
        private Vector3 rotation = Vector3.zero;
        private Vector3 velocity = Vector3.zero;

        private long lastTime;

        public Transform()
        {
            lastTime = GetCurrentUnixTimestampMillis();
        }

        public bool pendPositionUpdate(Vector3 position, Vector3 velocity, MessageUpdatePosition.Reason reason)
        {
            long newTime = GetCurrentUnixTimestampMillis();

            switch (reason)
            {
                case MessageUpdatePosition.Reason.STARTING:
                    if (this.position.Equals(Vector3.zero) && this.velocity.Equals(Vector3.zero))
                    {
                        lastTime = newTime;
                        return true;
                    }
                    else
                    {
                        lastTime = newTime;
                        return false;
                    }
                case MessageUpdatePosition.Reason.MOVEMENT:
                    float distance = Vector3.Distance(this.position, position);
                    Vector3 averageVelocity = (velocity + this.velocity) / 2;

                    float delta = (newTime - lastTime) / 1000.0f;

                    if (averageVelocity.magnitude * delta >= distance)
                    {
                        lastTime = newTime;
                        return true;
                    }
                    else
                    {
                        Connector.Events.Event.call(new EventLogging("Cancelled Movement Request for: " + averageVelocity.magnitude * delta + " and did " + distance));
                        lastTime = newTime;
                        return false;
                    }
                case MessageUpdatePosition.Reason.DEATH:
                case MessageUpdatePosition.Reason.TELEPORT:
                    lastTime = newTime;
                    return true;
                default:
                    return false;
            }
        }

        public bool pendRotationUpdate(Vector3 rotation)
        {
            return true;
        }

        public void updateTransform(Vector3 pos, Vector3 rotation, Vector3 velocity)
        {
            this.position = pos;
            this.velocity = velocity;
            this.rotation = rotation;
        }

        public float[] getPosition()
        {
            return Vector3Serialize.serialize(position);
        }

        public Vector3 getPositionV()
        {
            return position;
        }

        public float[] getRotation()
        {
            return Vector3Serialize.serialize(rotation);
        }

        public Vector3 getRotationV()
        {
            return rotation;
        }

        public float[] getVelocity()
        {
            return Vector3Serialize.serialize(velocity);
        }

        private static long GetCurrentUnixTimestampMillis()
        {
            return (long)(DateTime.UtcNow - UnixEpoch).TotalMilliseconds;
        }
    }
}
