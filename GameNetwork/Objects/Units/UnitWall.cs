﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameNetwork.Objects.Units
{
    public class UnitWall : Unit
    {
        public UnitWall(Team owner) : base(owner, NetworkType.TANK_WALL)
        {

        }
    }
}
