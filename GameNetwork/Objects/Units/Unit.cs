﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameNetwork.Objects.Units
{
    public abstract class Unit : GameObject
    {
        protected readonly Team owner;

        protected float unitMaxVelocity;

        public Unit(Team owner, NetworkType type) : base(GameServer.instance(), GameServer.instance().getObjectManagement(), type)
        {
            this.owner = owner;
            registerToTeam();
        }

        public void registerToTeam()
        {
            owner.addObject(this.getNetworkID());
        }
    }
}
