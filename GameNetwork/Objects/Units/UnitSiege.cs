﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameNetwork.Objects.Units
{
    public class UnitSiege : Unit
    {
        public UnitSiege(Team owner) : base(owner, NetworkType.TANK_SIEGE)
        {
        }
    }
}
