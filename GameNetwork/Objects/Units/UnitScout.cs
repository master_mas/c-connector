﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameNetwork.Objects.Units
{
    public class UnitScout : Unit
    {
        public UnitScout(Team owner) : base(owner, NetworkType.TANK_SCOUT)
        {

        }
    }
}
