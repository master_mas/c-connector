﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameNetwork.Objects.Building
{
    public abstract class Building : GameObject
    {
        protected readonly Team owner;

        public Building(Team owner, NetworkType type) : base(GameServer.instance(), GameServer.instance().getObjectManagement(), type)
        {
            this.owner = owner;
            registerToTeam();
        }

        public Team getTeam()
        {
            return owner;
        }

        public void registerToTeam()
        {
            owner.addObject(this.getNetworkID());
        }
    }
}
