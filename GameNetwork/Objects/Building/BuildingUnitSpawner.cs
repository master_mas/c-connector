﻿using System;
using System.Diagnostics;
using System.Timers;
using Connector.Events;
using GameNetwork.Events;
using GameNetwork.Objects.Units;
using GameNetwork.Packets;
using GameNetwork.Serialization;
using UnityEngine;
using Event = Connector.Events.Event;

namespace GameNetwork.Objects.Building
{
    public class BuildingUnitSpawner : Building, IDisposable
    {
        private readonly NetworkType unitType;
        private Timer spawnTimer;

        public BuildingUnitSpawner(Vector3 pos, NetworkType unitType, int unitSpawnTime, Team owner) : base(owner, NetworkType.BUILDING_UNIT_SPAWNER)
        {
            this.unitType = unitType;
            transform.updateTransform(pos, Vector3.zero, Vector3.zero);

            spawnTimer = new Timer(unitSpawnTime);
            spawnTimer.Elapsed += new ElapsedEventHandler(timerCallback);
            spawnTimer.AutoReset = true;
            spawnTimer.Start();
        }

        public void timerCallback(object state, ElapsedEventArgs e)
        {
            //TODO put player in this
            bool cancelled = Event.call(new EventUnitCreation(unitType, null)).isCancelled();
            if (!cancelled)
            {
                //Create Unit
                GameObject unit;
                switch (unitType)
                {
                    case NetworkType.TANK_SCOUT:
                        unit = new UnitScout(owner);
                        break;
                    case NetworkType.TANK_SIEGE:
                        unit = new UnitSiege(owner);
                        break;
                    case NetworkType.TANK_WALL:
                        unit = new UnitWall(owner);
                        break;
                    default:
                        return;
                }

                unit.getTransform().updateTransform(getTransform().getPositionV(), Vector3.zero, Vector3.zero);

                MessageCreateUnit obj = new MessageCreateUnit(
                    unit.getNetworkID(), 
                    owner.getTeamID(),
                    unitType, 
                    Vector3Serialize.deserialize(unit.getTransform().getPosition()), 
                    Vector3Serialize.deserialize(unit.getTransform().getRotation()),
                    networkID);
                server.broadcast(obj);
            }
            else
            {
                //TODO Unit Request Blocked
            }
        }

        public void newSpawnTimer(int unitSpawnTime)
        {
            spawnTimer.Interval = unitSpawnTime;
        }

        public override void cleanup()
        {
            spawnTimer.Stop();
        }

        public NetworkType getUnitType()
        {
            return unitType;
        }
    }
}
