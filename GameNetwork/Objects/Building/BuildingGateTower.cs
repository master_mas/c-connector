﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameNetwork.Objects.Building
{
    public class BuildingGateTower : Building
    {
        public BuildingGateTower(Team owner) : base(owner, NetworkType.BUILDING_TOWER)
        {

        }
    }
}
