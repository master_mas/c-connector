﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Connector.Events;
using GameNetwork.Packets;

namespace GameNetwork.Objects
{
    public class GameObject : IDisposable
    {
        protected readonly int networkID;
        protected readonly GameServer server;
        protected readonly ObjectManagement objectManagement;

        protected Transform transform = new Transform();
        protected readonly NetworkType type;
        protected Team team = null;

        private float health;

        public GameObject(GameServer server, ObjectManagement objectManagement, NetworkType type)
        {
            this.server = server;
            this.objectManagement = objectManagement;

            networkID = objectManagement.getID();
            objectManagement.registerObject(this);

            this.type = type;
        }

        public int getNetworkID()
        {
            return networkID;
        }

        public Transform getTransform()
        {
            return transform;
        }

        public Team getTeam()
        {
            return team;
        }

        public void Dispose()
        {
            objectManagement.unregisterObject(this);
            cleanup();
        }

        public void modHealth(float amount)
        {
            health += amount;
            if (health < 0)
            {
                death();
            }
            else
            {
                server.broadcast(new MessageHealthChange(networkID, amount));
            }
        }

        public NetworkType getNetworkType()
        {
            return type;
        }

        public void death()
        {
            server.broadcast(new MessageDestoryObject(networkID));

            objectManagement.unregisterObject(this);
            objectManagement.releaseID(networkID);

            cleanup();
        }

        public float Health => health;

        public void setTeam(Team team)
        {
            this.team = team;
        }

        public virtual void cleanup() { }
    }
}
