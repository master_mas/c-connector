﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Connector.Events;
using GameNetwork.Events;
using GameNetwork.Objects.Building;
using GameNetwork.Packets;
using UnityEngine;
using Event = Connector.Events.Event;
using EventHandler = Connector.Events.EventHandler;

namespace GameNetwork
{
    public class MatchController : IEventListener
    {
        private GameServer server;

        private int gameTime;
        private Team[] teams;

        private Timer secondTimer;
        private Dictionary<Player, bool> readyUp = new Dictionary<Player, bool>();

        public MatchController()
        {
            server = GameServer.instance();
            teams = server.getTeams();

            Event.registerListener(this);
        }

        public void startGame()
        {
            foreach (Player player in readyUp.Keys)
            {
                for (int i = 0; i < 3; i++)
                {
                    BuildingUnitSpawner spawner = new BuildingUnitSpawner(Vector3.zero, NetworkType.TANK_SCOUT, 5000, server.getTeam(player.getTeamID()));

                    MessageCreateFactory obj = new MessageCreateFactory(spawner.getNetworkID(), spawner.getTeam().getTeamID(),
                        spawner.getNetworkType(), spawner.getTransform().getPositionV(), spawner.getTransform().getRotationV(),
                        player.getPlayerID(), spawner.getUnitType(), true);

                    server.broadcast(obj);
                }

                for (int i = 0; i < 1; i++)
                {
                    BuildingUnitSpawner spawner = new BuildingUnitSpawner(Vector3.zero, NetworkType.TANK_SIEGE, 10000, server.getTeam(player.getTeamID()));

                    MessageCreateFactory obj = new MessageCreateFactory(spawner.getNetworkID(), spawner.getTeam().getTeamID(),
                        spawner.getNetworkType(), spawner.getTransform().getPositionV(), spawner.getTransform().getRotationV(),
                        player.getPlayerID(), spawner.getUnitType(), true);

                    server.broadcast(obj);
                }

                for (int i = 0; i < 1; i++)
                {
                    BuildingUnitSpawner spawner = new BuildingUnitSpawner(Vector3.zero, NetworkType.TANK_WALL, 20000, server.getTeam(player.getTeamID()));

                    MessageCreateFactory obj = new MessageCreateFactory(spawner.getNetworkID(), spawner.getTeam().getTeamID(),
                        spawner.getNetworkType(), spawner.getTransform().getPositionV(), spawner.getTransform().getRotationV(),
                        player.getPlayerID(), spawner.getUnitType(), true);

                    server.broadcast(obj);
                }

                for (int i = 0; i < 10; i++)
                {
                    BuildingGateTower tower = new BuildingGateTower(server.getTeam(player.getTeamID()));

                    MessageCreateObject obj = new MessageCreateObject(tower.getNetworkID(), tower.getTeam().getTeamID(),
                        tower.getNetworkType(), tower.getTransform().getPositionV(), tower.getTransform().getRotationV(),
                        player.getPlayerID());

                    server.broadcast(obj);
                }
            }
        }

        public void secondCallback(object state)
        {
            gameTime += 1;

            if (gameTime % 60 == 0)
            {
                minuteCallback(null);
            }

            server.broadcast(new MessageGameTime(gameTime));
        }

        public void minuteCallback(object state)
        {
            
        }

        public void readyUpPlayer(Player player)
        {
            readyUp[player] = true;

            new EventLogging("Player: " + player.getPlayerID() + " has readied up", selfLog: true);

            if (readyUp.Count <= 2)
            {
                foreach (bool test in readyUp.Values)
                {
                    if (!test)
                        return;
                }

                startGame();
            }
        }

        [EventHandler]
        public void playerJoinEvent(EventPlayerJoin e)
        {
            readyUp.Add(e.getPlayer(), false);
        }

        [EventHandler]
        public void playerLeaveEvent(EventPlayerQuit e)
        {
            readyUp.Remove(e.getPlayer());
        }
    }
}
