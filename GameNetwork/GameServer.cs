﻿using System;
using System.Threading;
using Connector;
using GameNetwork.Packets;
using Connector.Events;
using System.Collections.Generic;
using GameNetwork.EventHandlers;
using GameNetwork.Events;

namespace GameNetwork
{
    public class GameServer : IEventListener, IGameNetworkController
    {
        //Server Management
        private static GameServer gameServerInstance;
        private ConnectorServer connectorServer;
        private ObjectManagement objectManagement;
        private MatchController matchController;

        //Player Management
        private List<Player> players = new List<Player>();
        private Team[] teams = new Team[2];
        private int maxPlayers;

        //Event Handlers
        List<IEventListener> eventListeners = new List<IEventListener>();
        
        public GameServer() : this(34567, 34568) { }

        public GameServer(int tcpPort, int udpPort)
        {
            Event.registerListener(this);

            GameServer.gameServerInstance = this;
            connectorServer = new ConnectorServer(tcpPort, udpPort);
            objectManagement = new ObjectManagement();
            matchController = new MatchController();

            new MessageHandlerList(this, objectManagement);
            eventListeners.Add(new HandlerConnections(this));
            eventListeners.Add(new HandlerTeamManagement(this));

            teams[0] = new Team(this);
            teams[1] = new Team(this);

            startServer();

            Console.ReadLine();
            connectorServer.cleanup();
            Thread.Sleep(500);

            Environment.Exit(0);
        }

        public void startServer()
        {
            Thread thread = new Thread(new ThreadStart(connectorServer.run));
            thread.Start();
        }

        public static GameServer instance()
        {
            return gameServerInstance;
        }

        [Connector.Events.EventHandler]
        public void logging(EventLogging e)
        {
            Console.WriteLine(e);
        }

        public List<Player> getPlayers()
        {
            return players;
        }

        public Player getPlayer(int playerID)
        {
            foreach (Player player in players)
            {
                if (player.getPlayerID() == playerID)
                    return player;
            }

            return null;
        }

        public void addPlayer(Player player)
        {
            players.Add(player);
        }

        public void removePlayer(Player player)
        {
            players.Remove(player);
        }

        public ObjectManagement getObjectManagement()
        {
            return objectManagement;
        }

        public void broadcast(GameMessage message)
        {
            connectorServer.sendToAll(message);
        }

        public void broadcast(GameMessage message, Player avoid)
        {
            try
            {
                connectorServer.sendToAllExcept(message, avoid.getUnsafe().handle);
            }
            catch (NullReferenceException)
            {
                
            }
        }

        public void broadcast(GameMessage message, List<Player> avoid)
        {
            throw new NotImplementedException();
        }

        public Team[] getTeams()
        {
            return teams;
        }

        public Team getTeam(int teamID)
        {
            foreach (Team team in teams)
            {
                if (team.getTeamID() == teamID)
                {
                    return team;
                }
            }

            return null;
        }

        public MatchController getMatchController()
        {
            return matchController;
        }
    }
}
