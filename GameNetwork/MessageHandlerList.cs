﻿using GameNetwork.MessageHandlers;
using HandlerDestoryObject = GameNetwork.MessageHandlers.HandlerDestoryObject;
using HandlerUpdatePosition = GameNetwork.MessageHandlers.HandlerUpdatePosition;

namespace GameNetwork
{
    public class MessageHandlerList
    {
        public MessageHandlerList(GameNetwork.GameServer server, ObjectManagement objectManagement)
        {
            new HandlerUpdatePosition(server);
            new HandlerDestoryObject(server);
            new HandlerPlayerReady();
            new HandlerCreateBuildingRequest();
            new HandlerOwnPlayerDataRequest();
        }
    }
}
