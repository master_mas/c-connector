﻿using System;
using System.Threading;

namespace Connector
{
    class HeartBeat
    {
        private double waitingTime = 0;

        private Packets.MessageHeartBeat heartBeat;
        private IConnectionSender sender;
        private IRunnable runnable;
        private DateTime startOfTime;

        private Thread thread;

        public HeartBeat(IConnectionSender sender, IRunnable runnable)
        {
            heartBeat = new Packets.MessageHeartBeat();
            this.sender = sender;
            this.runnable = runnable;
            startOfTime = new DateTime(1970, 1, 1);

            thread = new Thread(new ThreadStart(sendBeat));
        }

        public void receivedHeatBeat(double sentTime)
        {
            waitingTime = DateTime.UtcNow.Subtract(startOfTime).TotalMilliseconds - sentTime;
        }

        public void sendBeat()
        {
            while(runnable.isRunning())
            {
                heartBeat.setTime(DateTime.UtcNow.Subtract(startOfTime).TotalMilliseconds);
                //Thread.Sleep(100);
//                sender.send(heartBeat);

                Thread.Sleep(5000);
            }
        }

        public double getPing()
        {
            return waitingTime;
        }
    }
}
