﻿using System;
using System.Net;
using System.Net.Sockets;
using Connector.Events;
using System.Threading;
using Connector.Packets;

namespace Connector
{
    public class ConnectorClient : ConnectorNetworkBase, IConnectionSender, IRunnable
    {
        private Socket tcpSocket;
        private UdpClient udpSocket;
        private IPAddress endPoint;

        private byte[] tcpBuffer;

        private HeartBeat heartBeat;

        private int searchPort = 34569;
        private UdpClient searchingClient;
        private bool searchEnabled = false;

        private int clientID = 0;

        public ConnectorClient() : this(34567, 34568) { }

        public ConnectorClient(int tcpPort, int udpPort) : base(tcpPort, udpPort)
        {
            tcpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            tcpBuffer = new byte[1024];
        }

        public void run()
        {
            if(endPoint == null)
            {
                Event.call(new EventLogging("No Endpoint Set", EventLogging.LogLevel.SEVERE));
                return;
            }

            try
            {
                tcpSocket.Connect(this.endPoint, tcpPort);

                udpSocket = new UdpClient();
                udpSocket.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                udpSocket.Connect(this.endPoint, udpPort);

                running = true;

                tcpResetReceive();
                udpResetReceive();

                heartBeat = new HeartBeat(this, this);
                heartBeat.sendBeat();

                Event.call(new EventConnectionToServer(this.endPoint));
            }
            catch (SocketException)
            {
                Event.call(new EventFailedToConnect(endPoint));
                running = false;
            }
            catch (NotSupportedException)
            {
                Event.call(new EventFailedToConnect(endPoint));
                running = false;
            }
        }

        private void tcpResetReceive()
        {
            if(running)
                tcpSocket.BeginReceive(tcpBuffer, 0, tcpBuffer.Length, SocketFlags.None, new AsyncCallback(tcpReceive), null);
        }

        private void udpResetReceive()
        {
            if (running)
            {
                udpSocket.BeginReceive(new AsyncCallback(udpReceive), null);
            }
        }

        private void tcpReceive(IAsyncResult result)
        {
            try
            {
                int recieved = tcpSocket.EndReceive(result);
                byte[] dataBuffer = new byte[recieved];
                Array.Copy(tcpBuffer, dataBuffer, recieved);

                ConnectorMessage obj = Serialization.deserialize(dataBuffer);
                handleMessage(obj, dataBuffer);
            }
            catch (System.ObjectDisposedException)
            {
                tcpResetReceive();
            }
            catch (System.Runtime.Serialization.SerializationException)
            {
                tcpResetReceive();
            }
            catch (SocketException)
            {
                Event.call(new EventServerDisconnect(EventDisconnect.DisconnectType.HARD));
                serviceShutdown();
            }
        }

        private void udpReceive(IAsyncResult result)
        {
            try
            {
                IPEndPoint remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
                byte[] dataBuffer = udpSocket.EndReceive(result, ref remoteEndPoint);

                udpResetReceive();

                ConnectorMessage obj = Serialization.deserialize(dataBuffer);
                handleMessage(obj, dataBuffer);
            }
            catch (ObjectDisposedException)
            {
                udpResetReceive();
                Event.call(new EventLogging("Dropped UDP Buffer", EventLogging.LogLevel.WARNING));
            }
            catch (System.Runtime.Serialization.SerializationException)
            {
                udpResetReceive();
                Event.call(new EventLogging("Dropped UDP Buffer", EventLogging.LogLevel.WARNING));
            }
        }

        public void handleMessage(ConnectorMessage message, byte[] original)
        {
//            Event.call(new EventLogging("Recieved Message: " + message.GetType().Name));

            if(message.getMessageID() >= Int32.MaxValue - 15)
            {
                switch(message.getMessageID())
                {
                    case (int)MessageID.SHUTDOWN:
                        running = false;
                        udpSocket.Close();
                        tcpSocket.Close();
                        Event.call(new EventServerDisconnect(EventDisconnect.DisconnectType.SOFT));
                        break;
                    case (int)MessageID.HEARTBEAT:
                        heartBeat.receivedHeatBeat(((Packets.MessageHeartBeat)Serialization.deserializeMessage(original)).getTime());
                        break;
                    case (int)MessageID.CLIENT_ID_HANDOVER:
                        clientID = message.getClientID();
                        send(new MessageUDPHandShake());
                        break;
                }
            }
            else if (handlers.ContainsKey(message.getMessageID()))
            {
                try
                {
                    handlers[message.getMessageID()].DynamicInvoke(message.getMessageID(), Serialization.deserializeMessage(original), this);
                }
                catch (Exception e)
                {
                    Event.call(new EventLogging(e.ToString(), EventLogging.LogLevel.SEVERE));
                }
            }
            else
            {
                Event.call(new EventLogging("Unknown Message ID: " + message.getMessageID(), EventLogging.LogLevel.WARNING));
            }

            if (message.getBackBone())
                tcpResetReceive();
//            else
//                udpResetReceive();
        }

        public void setEndPoint(string endPoint)
        {
            if(endPoint.Contains("localhost") || endPoint.Contains("127.0.0.1"))
            {
                this.endPoint = IPAddress.Loopback;
            }
            else if (!IPAddress.TryParse(endPoint, out this.endPoint))
            {
                try
                {
                    this.endPoint = Dns.GetHostAddresses(endPoint)[0];
                }
                catch(SocketException)
                {
                    running = false;
                    endPoint = null;
                    Event.call(new EventLogging("Failed to Parse End Point", EventLogging.LogLevel.SEVERE));
                }
            }
        }

        public void send(ConnectorMessage complexObject)
        {
            if (!running)
            {
                Event.call(new EventLogging("Connetor Client not running. Failed to send: " + complexObject.GetType().Name, EventLogging.LogLevel.WARNING));
                return;
            }

            if (!complexObject.GetType().IsSerializable)
            {
                Event.call(new EventLogging("Packet not Serialable: " + complexObject.GetType().Name, EventLogging.LogLevel.WARNING));
                return;
            }

            complexObject.setClientID(clientID);

            byte[] data = Serialization.serialize(complexObject);

            try
            {
                if (complexObject.getBackBone())
                {
                    tcpSocket.Send(data);
                }
                else
                {
                    udpSocket.Send(data, data.Length);
                }
            }
            catch (SocketException)
            {
                Event.call(new EventServerDisconnect(EventDisconnect.DisconnectType.HARD));
                serviceShutdown();
            }
            
        }

        public override void cleanup()
        {
            Packets.MessageShutdown shutdown = new Packets.MessageShutdown();
            shutdown.setBackBone(true);
            send(shutdown);

            serviceShutdown();
        }
        
        private void serviceShutdown()
        {
            running = false;

            try
            {
                udpSocket.Close();
                tcpSocket.Close();
            }
            catch (NullReferenceException) { }
        }

        public double getPing()
        {
            return heartBeat.getPing();
        }

        public void searchForServers(bool enabled)
        {
            if(searchEnabled != enabled)
            {
                searchEnabled = enabled;

                if (searchEnabled)
                {
                    searchingClient = new UdpClient();
                    searchingClient.Client.Bind(new IPEndPoint(IPAddress.Any, searchPort));

                    resetSearchReceive();

                    Event.call(new EventLogging("Client Searcher Enabled"));
                }
                else
                {
                    searchingClient.Close();
                    Event.call(new EventLogging("Client Searcher Enabled"));
                }
            }
        }

        private void resetSearchReceive()
        {
            searchingClient.BeginReceive(searchReceive, null);
        }

        private void searchReceive(IAsyncResult result)
        {
            try
            {
                IPEndPoint endpoint = null;
                byte[] data = searchingClient.EndReceive(result, ref endpoint);

                Event.call(new EventDiscoveredServer(endpoint.Address, null, 0, 0));

                resetSearchReceive();
            }
            catch (SocketException) { }
        }

        public void setSearchPort(int port)
        {
            this.searchPort = port;
        }
    }
}
