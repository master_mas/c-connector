﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Connector.Events;
using Connector.Packets;

namespace Connector
{
    public class ConnectorClientHandle : IConnectionSender
    {
        private Socket tcpSocket;
        private UdpClient udpSocket;
        private int udpPort;
        private byte[] tcpBuffer;
        private Dictionary<int, Delegate> messageHandlers;
        private IPEndPoint udpEndpoint = null;
        private ConnectorServer serverMaster;
        private bool running = true;
        private int clientID;

        public ConnectorClientHandle(
            int udpPort,
            Socket tcpSocket,
            ref Dictionary<int, Delegate> messageHandlers,
            ref UdpClient udpSocket,
            ConnectorServer serverMaster,
            int clientID)
        {
            this.clientID = clientID;
            this.udpPort = udpPort;

            this.tcpSocket = tcpSocket;
            this.udpSocket = udpSocket;

            tcpBuffer = new byte[1024];

            send(new MessageClientIDHandOver());

            this.messageHandlers = messageHandlers;

            this.serverMaster = serverMaster;
            tcpResetRecieve();
        }

        private void tcpResetRecieve()
        {
            if(running)
                tcpSocket.BeginReceive(tcpBuffer, 0, tcpBuffer.Length, SocketFlags.None, new AsyncCallback(tcpReceive), null);
        }

        private void tcpReceive(IAsyncResult result)
        {
            try
            {
                int recieved = tcpSocket.EndReceive(result);
                byte[] dataBuffer = new byte[recieved];
                Array.Copy(tcpBuffer, dataBuffer, recieved);

                ConnectorMessage obj = Serialization.deserialize(dataBuffer);
                handleMessage(obj, dataBuffer, (IPEndPoint)tcpSocket.RemoteEndPoint);
            }
            catch(ObjectDisposedException)
            {
                tcpResetRecieve();
            }
            catch (System.Runtime.Serialization.SerializationException)
            {
                tcpResetRecieve();
            }
            catch(SocketException)
            {
                Event.call(new EventClientDisconnect(getEndpoint().Address, clientID, EventDisconnect.DisconnectType.HARD));
                close();
            }
        }

        public void handleMessage(ConnectorMessage message, byte[] original, IPEndPoint remoteIP)
        {
            if (message.getMessageID() >= Int32.MaxValue - 15)
            {
                switch (message.getMessageID())
                {
                    case (int)MessageID.UDP_HANDSHAKE:
                        udpEndpoint = remoteIP;
                        Event.call(new EventLogging("New Connection Full UDP Address: " + getEndpoint()));
                        Event.call(new EventClientConnection(getEndpoint().Address, this));
                        break;
                    case (int)MessageID.SHUTDOWN:
                        close();
                        break;
                    case (int)MessageID.HEARTBEAT:
                        send(message);
                        break;
                }
            }
            else if (messageHandlers.ContainsKey(message.getMessageID()))
            {
//                Event.call(new EventLogging("New Message with ID: " + message.getMessageID() + " from: " + remoteIP));
                messageHandlers[message.getMessageID()].DynamicInvoke(message.getMessageID(), Serialization.deserializeMessage(original), this);
            }
            else
            {
                Event.call(new EventLogging("Unknown Message ID: " + message.getMessageID(), EventLogging.LogLevel.WARNING));
            }

            if (message.getBackBone())
                tcpResetRecieve();
        }

        public void send(ConnectorMessage complexObject)
        {
            if (!running)
                return;

            if(!complexObject.GetType().IsSerializable)
            {
                Event.call(new EventLogging("Packet not Serialable: " + complexObject.GetType().Name));
                return;
            }

            complexObject.setClientID(clientID);

            byte[] data = Serialization.serialize(complexObject);

            Event.call(new EventLogging("Sending Packet: " + complexObject.GetType().Name));

            if (complexObject.getBackBone())
            {
                tcpSocket.Send(data);
            }
            else
            {
                Event.call(new EventLogging("Attempting to send UDP Packet (" + complexObject.GetType().Name + ") to " + getEndpoint()));
                if(getEndpoint() != null)
                    udpSocket.Send(data, data.Length, getEndpoint());
                else
                {
                    Event.call(new EventLogging("Failed to send UDP Packet to " + getEndpoint() + ". Endpoint Null"));
                }
            }
        }

        public void close()
        {
            try
            {
                tcpSocket.Close();
            }
            catch(NullReferenceException) { }
            
            running = false;

            serverMaster.removeClientHandle(this);
        }

        public IPEndPoint getEndpoint()
        {
            return udpEndpoint;
        }

        public int getClientID()
        {
            return clientID;
        }

        /*public override bool Equals(object obj)
        {
            try
            {
                ConnectorClientHandle handle = (ConnectorClientHandle) obj;
                if (handle.getEndpoint().Equals(this.getEndpoint()))
                {
                    return true;
                }

                return false;
            }
            catch (InvalidCastException)
            {
                return false;
            }
        }*/
    }
}
