﻿using System;
using System.Collections.Generic;

namespace Connector
{
    public abstract class ConnectorNetworkBase
    {
        private static ConnectorNetworkBase internalInstance;

        protected int tcpPort;
        protected int udpPort;
        protected Dictionary<int, Delegate> handlers;
        protected bool running = false;

        public ConnectorNetworkBase(int tcpPort, int udpPort)
        {
            ConnectorNetworkBase.internalInstance = this;

            this.tcpPort = tcpPort;
            this.udpPort = udpPort;
            handlers = new Dictionary<int, Delegate>();
        }

        public void registerMessageHandler(int id, Delegate access)
        {
            handlers.Add(id, access);
        }

        public abstract void cleanup();

        public bool isRunning()
        {
            return running;
        }

        public static ConnectorNetworkBase instance()
        {
            return internalInstance;
        }
    }
}
