﻿using System.Linq;

namespace Connector
{
    public abstract class ConnectorMessageHandler<T> /*where T : ConnectorMessage*/
    {
        private int id;
        public delegate void HandleMessage(int id, T data, IConnectionSender sender);
        private HandleMessage messageDelegate;

        public ConnectorMessageHandler(int id)
        {
            this.id = id;
            messageDelegate = new HandleMessage(handleMessage);
        }

        public int getID()
        {
            return id;
        }

        public void handleMessage(int id, object data, IConnectionSender sender)
        {
            if (data.GetType().GetInterfaces().Contains(typeof(Connector.Events.ICancellable)))
            {
                if (((Connector.Events.ICancellable)data).isCancelled())
                {
                    return;
                }
            }

            handleMessage(id, (T)data, sender);
        }

        public abstract void handleMessage(int id, T data, IConnectionSender sender);

        public HandleMessage getDelegate()
        {
            return messageDelegate;
        }
    }
}
