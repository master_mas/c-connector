﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace Connector.Events
{
    public class EventConnectionToServer : Event
    {
        private static EventHandlerContainer hc = new EventHandlerContainer();

        private IPAddress address;

        public EventConnectionToServer(IPAddress address)
        {
            this.address = address;
        }

        public IPAddress GetAddress()
        {
            return address;
        }
    }
}
