﻿using System.Collections.Generic;
using System.Reflection;

namespace Connector.Events
{
    public class EventHandlerContainer
    {
        public Dictionary<EventHandler.EventPriority, Dictionary<IEventListener, MethodInfo>> handlers = new Dictionary<EventHandler.EventPriority, Dictionary<IEventListener, MethodInfo>>();
    }
}
