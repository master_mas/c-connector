﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Connector.Events
{
    public class Event
    {
        public static void registerListener(IEventListener listener)
        {
            foreach (MethodInfo info in listener.GetType().GetMethods())
            {

                //IEnumerable<Attribute> attributes = info.GetCustomAttributes(typeof(EventHandler));
                object[] attributes = info.GetCustomAttributes(false);
                if (attributes.Any())
                {
                    if (attributes[0].GetType() == typeof(EventHandler))
                    {
                        ParameterInfo[] pInfo = info.GetParameters();

                        if (pInfo.Length == 1)
                        {
                            FieldInfo[] fInfo = pInfo[0].ParameterType.GetFields(BindingFlags.NonPublic | BindingFlags.Static);
                            EventHandlerContainer hc = null;
                            foreach (FieldInfo propertyInfo in fInfo)
                            {
                                if (propertyInfo.GetValue(null).GetType() == typeof(EventHandlerContainer))
                                {
                                    hc = (Connector.Events.EventHandlerContainer)propertyInfo.GetValue(null);
                                    break;
                                }
                            }

                            if (hc == null)
                            {
                                if(pInfo[0].GetType() != typeof(System.Reflection.ParameterInfo))
                                    Event.call(new EventLogging("Trying to Register Listener to Event '" + pInfo[0].GetType() + "' and it has no HandlerContainer!", EventLogging.LogLevel.WARNING));
                                continue;
                            }

                            EventHandler.EventPriority priority = ((EventHandler)attributes.First()).getEventPriority();

                            if (!hc.handlers.ContainsKey(priority))
                            {
                                hc.handlers.Add(priority, new Dictionary<IEventListener, MethodInfo>());
                            }

                            try
                            {
                                hc.handlers[priority].Add(listener, info);
                            }
                            catch (Exception)
                            {
                                Event.call(new EventLogging("More than one method handling the same event in the same class [Class: " + listener.GetType().Name + ", Method: " + info.Name + "]", EventLogging.LogLevel.SEVERE));
                            }
                        }
                    }
                }
            }
        }

        public static void removeListener(IEventListener listener)
        {
            foreach (MethodInfo info in listener.GetType().GetMethods())
            {
                //IEnumerable<Attribute> attributes = info.GetCustomAttributes(typeof(EventHandler));
                object[] attributes = info.GetCustomAttributes(false);
                if (attributes.Any())
                {
                    if (attributes[0].GetType() == typeof(EventHandler))
                    {
                        ParameterInfo[] pInfo = info.GetParameters();

                        if (pInfo.Length == 1)
                        {
                            FieldInfo[] fInfo = pInfo[0].ParameterType.GetFields(BindingFlags.NonPublic | BindingFlags.Static);

                            EventHandlerContainer hc = null;
                            foreach (FieldInfo propertyInfo in fInfo)
                            {
                                if (propertyInfo.GetValue(null).GetType() == typeof(EventHandlerContainer))
                                {
                                    hc = (Connector.Events.EventHandlerContainer)propertyInfo.GetValue(null);
                                    break;
                                }
                            }

                            if (hc == null)
                            {
                                Event.call(new EventLogging("Trying to Unregister Listener to Event '" + pInfo[0].GetType() + "' and it can't access HandlerContainer!", EventLogging.LogLevel.WARNING));
                                continue;
                            }

                            EventHandler.EventPriority priority = ((EventHandler)attributes.First()).getEventPriority();

                            hc.handlers[priority].Remove(listener);
                        }
                    }
                }
            }
        }

        private static readonly EventHandler.EventPriority[] order = {
                EventHandler.EventPriority.HIGH,
                EventHandler.EventPriority.NORMAL,
                EventHandler.EventPriority.LOW
            };

        public static T call<T>(T data) where T : Event
        {
            EventHandlerContainer hc = null;
            FieldInfo[] pInfo = typeof(T).GetFields(BindingFlags.NonPublic | BindingFlags.Static);
            foreach(FieldInfo info in pInfo)
            {
                if(info.GetValue(null).GetType() == typeof(EventHandlerContainer))
                {
                    hc = (EventHandlerContainer)info.GetValue(null);
                    break;
                }
            }

            if(hc == null)
            {
                Event.call(new EventLogging("Event '" + data.GetType() + "' was aborted: No Handler Container", EventLogging.LogLevel.WARNING));
                return data;
            }

            foreach (EventHandler.EventPriority priority in order)
            {
                if (hc.handlers.ContainsKey(priority))
                {
                    callEventAction<T>(data, priority, hc);
                }
            }

            return data;
        }

        private static T callEventAction<T>(T data, EventHandler.EventPriority priority, EventHandlerContainer hc)
        {
            ICancellable cancellable = null;

            if (data.GetType().GetInterfaces().Contains(typeof(ICancellable)))
            {
                cancellable = (ICancellable) data;
            }

            List<IEventListener> listners = new List<IEventListener>(hc.handlers[priority].Keys);
            foreach (IEventListener listener in listners)
            {
                if (cancellable != null && cancellable.isCancelled())
                {
                    return data;
                }

                try
                {
                    hc.handlers[priority][listener].Invoke(listener, new object[] { data });
                }
                catch (TargetInvocationException e)
                {
                    Event.call(new EventLogging("Invoke Exception for: " + data.GetType().Name));
                }
            }

            return data;
        }
    }
}
