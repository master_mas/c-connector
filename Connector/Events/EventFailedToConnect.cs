﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace Connector.Events
{
    public class EventFailedToConnect : Event
    {
        private static EventHandlerContainer hc = new EventHandlerContainer();

        private IPAddress ipAddress;

        public EventFailedToConnect(IPAddress endPoint)
        {
            this.ipAddress = endPoint;

            Event.call(new EventLogging("Failed to connect to Server: " + ipAddress, EventLogging.LogLevel.SEVERE));
        }

        public IPAddress GetIpAddress()
        {
            return ipAddress;
        }
    }
}
