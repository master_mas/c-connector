﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace Connector.Events
{
    public class EventDiscoveredServer : Event
    {
        private static EventHandlerContainer hc = new EventHandlerContainer();

        private string serverName;
        private int currentPlayers;
        private int maxPlayers;
        private IPAddress ipAddress;

        public EventDiscoveredServer(IPAddress ipAddress, string serverName, int currentPlayers, int maxPlayers)
        {
            this.ipAddress = ipAddress;
            this.serverName = serverName;
            this.currentPlayers = currentPlayers;
            this.maxPlayers = maxPlayers;
        }

        public string getServerName()
        {
            return serverName;
        }

        public int getCurrentPlayers()
        {
            return currentPlayers;
        }

        public int getMaxPlayers()
        {
            return maxPlayers;
        }

        public IPAddress getIPAddress()
        {
            return ipAddress;
        }
    }
}
