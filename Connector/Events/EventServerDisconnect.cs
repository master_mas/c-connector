﻿namespace Connector.Events
{
    public class EventServerDisconnect : EventDisconnect
    {
        private static EventHandlerContainer hc = new EventHandlerContainer();

        public EventServerDisconnect(DisconnectType type) : base(type) { }
    }
}
