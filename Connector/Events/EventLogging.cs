﻿using System;
using System.Reflection;

namespace Connector.Events
{
    public class EventLogging : Event, ICancellable
    {
        private static EventHandlerContainer hc = new EventHandlerContainer();

        private static LogLevel minLoggingLevel = LogLevel.INFO;

        public static void setMinLoggingLevel(LogLevel level)
        {
            minLoggingLevel = level;
        }

        public enum LogLevel
        {
            INFO = 1,
            WARNING = 2,
            SEVERE = 3,
            NONE = Int32.MaxValue
        };

        private string logMessage;
        private LogLevel logLevel;
        private object relation = null;
        private bool cancelled = false;

        public EventLogging(string message, LogLevel level = LogLevel.INFO, object relation = null, bool selfLog = false)
        {
            this.logMessage = message;
            this.logLevel = level;
            this.relation = relation;

            if (level < minLoggingLevel || level == LogLevel.NONE)
                setCancelled(true);

            if (selfLog)
                Event.call(this);
        }

        public EventLogging(object obj, LogLevel level = LogLevel.INFO, object relation = null, bool selfLog = false)
        {
            Type type = obj.GetType();
            this.logMessage = type.Name;
            this.logMessage += "[";
            bool first = true;

            foreach (FieldInfo field in type.GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic))
            {
                if (!first)
                    this.logMessage += ", ";

                this.logMessage += field.Name;
                this.logMessage += ": ";
                this.logMessage += field.GetValue(obj);

                first = false;
            }

            this.logMessage += "]";

            this.logLevel = level;
            this.relation = relation;

            if (level < minLoggingLevel || level == LogLevel.NONE)
                setCancelled(true);

            if (selfLog)
                Event.call(this);
        }

        public string getLogMessage()
        {
            return logMessage;
        }

        public LogLevel getLogLevel()
        {
            return logLevel;
        }

        public object getRelation()
        {
            return relation;
        }

        public override string ToString()
        {
            return "[" + DateTime.Now.ToString("HH:mm:ss") + " " + logLevel + "]: " + logMessage;
        }

        public bool isCancelled()
        {
            return cancelled;
        }

        public void setCancelled(bool cancel)
        {
            cancelled = cancel;
        }
    }
}
