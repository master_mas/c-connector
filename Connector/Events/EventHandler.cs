﻿using System;

namespace Connector.Events
{
    [AttributeUsage(AttributeTargets.Method , AllowMultiple = true)]
    public class EventHandler : System.Attribute
    {
        public enum EventPriority
        {
            LOW = 1,
            NORMAL = 2,
            HIGH = 3
        }

        private EventPriority priority;

        public EventHandler() : this(EventPriority.NORMAL) { }

        public EventHandler(EventPriority priority)
        {
            this.priority = priority;
        }

        public EventPriority getEventPriority()
        {
            return priority;
        }
    }
}
