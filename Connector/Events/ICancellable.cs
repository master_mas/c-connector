﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Connector.Events
{
    public interface ICancellable
    {

        bool isCancelled();

        void setCancelled(bool cancel);
    }
}
