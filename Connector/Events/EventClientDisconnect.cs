﻿using System.Net;

namespace Connector.Events
{
    public class EventClientDisconnect : EventDisconnect
    {
        private static EventHandlerContainer hc = new EventHandlerContainer();

        private IPAddress ipAddress;
        private int clientID;

        public EventClientDisconnect(IPAddress address, int clientID, DisconnectType type) : base(type)
        {
            this.ipAddress = address;
            this.clientID = clientID;
        }

        public IPAddress getIPAddress()
        {
            return ipAddress;
        }

        public int getClientID()
        {
            return clientID;
        }
    }
}
