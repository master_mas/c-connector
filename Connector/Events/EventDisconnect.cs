﻿namespace Connector.Events
{
    public class EventDisconnect : Event
    {
        public enum DisconnectType
        {
            SOFT = 1,
            HARD = 2,
        };

        private DisconnectType type;

        public EventDisconnect(DisconnectType type)
        {
            this.type = type;
        }

        public DisconnectType getDisconnectType()
        {
            return type;
        }
    }
}
