﻿using System.Net;

namespace Connector.Events
{
    public class EventClientConnection : Event
    {
        private static EventHandlerContainer hc = new EventHandlerContainer();

        private IPAddress address;
        private ConnectorClientHandle clientHandle;

        public EventClientConnection(IPAddress ipAddress, ConnectorClientHandle handle)
        {
            this.address = ipAddress;
            this.clientHandle = handle;
        }

        public IPAddress getIPAddress()
        {
            return address;
        }

        public ConnectorClientHandle getClientHandle()
        {
            return clientHandle;
        }
    }
}
