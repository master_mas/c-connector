﻿using System;

namespace Connector
{
    [Serializable]
    public class ConnectorMessage
    {
        private int messageID;
        private bool backBone;
        private int clientID;

        public ConnectorMessage(int messageID)
        {
            this.messageID = messageID;
            backBone = false;
        }

        public void setBackBone(bool backBone)
        {
            this.backBone = backBone;
        }

        public bool getBackBone()
        {
            return backBone;
        }

        public void setMessageID(int messageID)
        {
            this.messageID = messageID;
        }

        public int getMessageID()
        {
            return messageID;
        }

        public int getClientID()
        {
            return clientID;
        }

        public void setClientID(int id)
        {
            this.clientID = id;
        }
    }
}
