﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Net;
using Connector.Events;
using System.Threading;

namespace Connector
{
    public class ConnectorServer : ConnectorNetworkBase
    {
        private Socket serverSocket;
        private UdpClient udpSocket;
        private List<ConnectorClientHandle> clientSockets = new List<ConnectorClientHandle>();
        private Queue<int> pendingClientIDs = new Queue<int>();
        private int clientIDsUpTo = 0;

        private volatile UdpClient broadcastSocket;
        private Thread broadcastRefresher;
        private volatile int broadcastPort = 34569;
        private volatile ConnectorMessage broadcastData = null;
        private volatile bool broadcastEnabled = false;

        public ConnectorServer() : this(34567, 34568) { }

        public ConnectorServer(int tcpPort, int udpPort) : base(tcpPort, udpPort)
        {
            serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            udpSocket = new UdpClient(base.udpPort);
            udpSocket.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
        }

        public void run()
        {
            serverSocket.Bind(new IPEndPoint(IPAddress.Any, tcpPort));
            //5 Max Queued Clients. Probably set to max players?
            serverSocket.Listen(5);

            running = true;

            setAcceptingTCP();

            udpReceiveReset();
        }

        private void udpReceiveReset()
        {
            if(running)
                try
                {
                    udpSocket.BeginReceive(new AsyncCallback(udpPacketRequest), null);
//                    Event.call(new EventLogging("UDP Reset Passed"));
                }
                catch (SocketException)
                {
                    Event.call(new EventLogging("UDP Reset Failed. Dumping Packets", EventLogging.LogLevel.SEVERE));
                    udpReceiveReset();
                }
        }

        private void setAcceptingTCP()
        {
            if(running)
                serverSocket.BeginAccept(new AsyncCallback(acceptCallback), null);
        }

        private void acceptCallback(IAsyncResult result)
        {
            try
            {
                Socket socket = serverSocket.EndAccept(result);
                ConnectorClientHandle handle = new ConnectorClientHandle(udpPort, socket, ref handlers, ref udpSocket, this, (pendingClientIDs.Count > 0 ? pendingClientIDs.Dequeue() : clientIDsUpTo++));
                clientSockets.Add(handle);
            }
            catch(ObjectDisposedException) { }

            setAcceptingTCP();
        }

        private void udpPacketRequest(IAsyncResult result)
        {
            try
            {
                IPEndPoint remote = new IPEndPoint(IPAddress.Any, 0);
                byte[] data = udpSocket.EndReceive(result, ref remote);

                ConnectorMessage message = Serialization.deserialize(data);
                foreach (ConnectorClientHandle handle in clientSockets)
                {
                    if (handle.getClientID() == message.getClientID())
                    {
                        handle.handleMessage(message, data, remote);
                    }
                }
            }
            catch(ObjectDisposedException) { }
            catch (System.Runtime.Serialization.SerializationException) { }

            udpReceiveReset();
        }

        public void sendToAll(ConnectorMessage message)
        {
            if (!running)
                return;

            foreach(ConnectorClientHandle handle in clientSockets)
            {
                handle.send(message);
            }
        }

        public void sendToAllExcept(ConnectorMessage message, IConnectionSender sender)
        {
            if (!running)
                return;

            ConnectorClientHandle senderHandle = (ConnectorClientHandle) sender;
            foreach (ConnectorClientHandle handle in clientSockets)
            {
                if (handle.getClientID() != senderHandle.getClientID())
                {
                    handle.send(message);
                }
            }
        }

        public void send(ConnectorMessage message, IConnectionSender sender)
        {
            if (!running)
                return;

            sender.send(message);
        }

        public void removeClientHandle(ConnectorClientHandle connectorClientHandle)
        {
            Event.call(new EventClientDisconnect(connectorClientHandle.getEndpoint().Address, connectorClientHandle.getClientID(), EventDisconnect.DisconnectType.SOFT));
            pendingClientIDs.Enqueue(connectorClientHandle.getClientID());
            clientSockets.Remove(connectorClientHandle);
        }

        public override void cleanup()
        {
            Packets.MessageShutdown shutdown = new Packets.MessageShutdown();
            shutdown.setBackBone(true);
            sendToAll(shutdown);

            running = false;

            try
            {
                udpSocket.Close();
                serverSocket.Close();
            }
            catch (NullReferenceException) { }


            List<ConnectorClientHandle> l = new List<ConnectorClientHandle>();
            l.AddRange(clientSockets);
            foreach (ConnectorClientHandle handle in l)
            {
                handle.close();
            }

            clientSockets.Clear();
        }

        public void setBroadCastPort(int port)
        {
            this.broadcastPort = port;
        }

        public void broadcast(bool enabled)
        {
            if(broadcastEnabled != enabled)
            {
                broadcastEnabled = enabled;

                if (enabled)
                {
                    broadcastSocket = new UdpClient();

                    broadcastRefresher = new Thread(new ThreadStart(broadcastPacket));
                    broadcastRefresher.Start();

                    Event.call(new EventLogging("Server Broadcast Enabled"));
                }
                else
                {
                    broadcastRefresher.Interrupt();
                    broadcastSocket.Close();
                    broadcastSocket = null;

                    Event.call(new EventLogging("Server Broadcast Disabled"));
                }
            }
        }

        private void broadcastPacket()
        {
            try
            {
                while (broadcastEnabled)
                {
                    if (broadcastData != null)
                    {
                        byte[] data = Serialization.serialize(broadcastData);

                        broadcastSocket.Send(data, data.Length, new IPEndPoint(IPAddress.Broadcast, broadcastPort));
                    }

                    Thread.Sleep(1500);
                }
            }
            catch (ThreadInterruptedException) { }
        }

        public void setBroadCastPacket(ConnectorMessage message)
        {
            if(message.GetType().IsSerializable)
            {
                broadcastData = message;
            }
            else
            {
                Event.call(new EventLogging("Broadcast Message not Serializable", EventLogging.LogLevel.WARNING));
            }
        }

        
    }
}
