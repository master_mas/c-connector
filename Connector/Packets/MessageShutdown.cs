﻿using System;

namespace Connector.Packets
{
    [Serializable]
    class MessageShutdown : ConnectorMessage
    {
        public MessageShutdown() : base((int) MessageID.SHUTDOWN) { }
    }
}
