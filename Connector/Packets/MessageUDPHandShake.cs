﻿using System;

namespace Connector.Packets
{
    [Serializable]
    class MessageUDPHandShake : ConnectorMessage
    {
        public MessageUDPHandShake() : base((int) MessageID.UDP_HANDSHAKE) { }
    }
}
