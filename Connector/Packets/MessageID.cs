﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Connector.Packets
{
    enum MessageID
    {
        UDP_HANDSHAKE = Int32.MaxValue,
        SHUTDOWN = Int32.MaxValue - 1,
        HEARTBEAT = Int32.MaxValue - 2,
        BROADCAST = Int32.MaxValue - 4,
        CLIENT_ID_HANDOVER = Int32.MaxValue - 5,

    }
}
