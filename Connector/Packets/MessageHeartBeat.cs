﻿using System;

namespace Connector.Packets
{
    [Serializable]
    class MessageHeartBeat : ConnectorMessage
    {
        private double sentTime;

        public MessageHeartBeat() : base((int) MessageID.HEARTBEAT)
        {
            setBackBone(true);
        }

        public void setTime(double time)
        {
            this.sentTime = time;
        }

        public double getTime()
        {
            return sentTime;
        }
    }
}
