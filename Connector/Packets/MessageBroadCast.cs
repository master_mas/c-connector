﻿using System;

namespace Connector.Packets
{
    [Serializable]
    public class MessageBroadCast : ConnectorMessage
    {
        public MessageBroadCast() : base ((int)MessageID.BROADCAST) { }
    }
}
