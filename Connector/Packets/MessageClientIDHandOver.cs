﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Connector.Packets
{
    [Serializable]
    public class MessageClientIDHandOver : ConnectorMessage
    {
        public MessageClientIDHandOver() : base((int)MessageID.CLIENT_ID_HANDOVER) { setBackBone(true); }
    }
}
