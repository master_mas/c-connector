﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Connector
{
    class Serialization
    {
        public static byte[] serialize(ConnectorMessage serializableObject)
        {
            try
            {
                using (var memoryStream = new MemoryStream())
                {
                    (new BinaryFormatter()).Serialize(memoryStream, serializableObject);
                    return memoryStream.ToArray();
                }
            }
            catch (System.Runtime.Serialization.SerializationException) { throw; }
        }

        public static object deserializeMessage(byte[] data)
        {
            try
            {
                using (var memoryStream = new MemoryStream(data))
                    return (new BinaryFormatter()).Deserialize(memoryStream);
            }
            catch (System.Runtime.Serialization.SerializationException) { throw; }
        }

        public static ConnectorMessage deserialize(byte[] data) 
        {
            try
            {
                using (var memoryStream = new MemoryStream(data))
                    return (ConnectorMessage)(new BinaryFormatter()).Deserialize(memoryStream);
            }
            catch (System.Runtime.Serialization.SerializationException) { throw; }
        }
    }
}
