﻿namespace Connector
{
    interface IRunnable
    {
        bool isRunning();
    }
}
