﻿namespace Connector
{
    public interface IConnectionSender
    {
        void send(ConnectorMessage complexObject);
    }
}
